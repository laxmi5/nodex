FROM node:slim

RUN apt update -y && apt install -y git

COPY . .

RUN chmod +x task.sh

RUN ls

ENTRYPOINT ["./task.sh"]
